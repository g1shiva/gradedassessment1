package com.hcl.collections;

import java.util.ArrayList;
import java.util.List;

public class Book {
	
	int id;
	String name;
	Double price;
	String genre;
	int noOfCopiesSold;
	String bookstatus;
	
	//isNumber is method to check the given input is integer or String.
	static boolean isNumber(String name)
    {
        for (int i = 0; i < name.length(); i++)
            if (Character.isDigit(name.charAt(i)) == false)
                return false;
 
        return true;
    }
	
	
	//getter and setter methods.
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		if(id<=0)
		{
			throw new IllegalArgumentExceptionDemo("Exception occured, Id cannot less than or equal to zero");			
		}else {
			this.id = id;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
	
		if(isNumber(name)) {
        	throw new IllegalArgumentExceptionDemo("Exception occured, Name can be String only.");
        }
		else 
		{
		this.name=name;
		}
	}
		
	
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		if(price<=0)
		{
			throw new IllegalArgumentExceptionDemo("Exception occured, Price cannot less than or equal to zero");			
		}
		else {
		this.price = price;
	}}
	
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		if(isNumber(genre)) {
        	throw new IllegalArgumentExceptionDemo("Exception occured, Genre can be String only.");
        }
		else 
		{	
		this.genre = genre;
		}
	}
	
	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}
	public void setNoOfCopiesSold(int noOfCopiesSold) {
		
		if(noOfCopiesSold<=0)
		{
			throw new IllegalArgumentExceptionDemo("Exception occured, NoOfCopiesSold cannot less than or equal to zero");			
		}
		else {
			this.noOfCopiesSold = noOfCopiesSold;
								}
		}
	
	public String getBookstatus() {
		return bookstatus;
	}
	public void setBookstatus(String bookstatus) {
		if(isNumber(bookstatus)) {
        	throw new IllegalArgumentExceptionDemo("Exception occured, BookStatus can be String only.");
        }	
		else 
		{	
		this.bookstatus = bookstatus;
		}
	}
	
	@Override
	public String toString() {
		return id+"\t"+ name+"\t"+ price+"\t"  + genre+"\t     " + noOfCopiesSold +"\t\t" + bookstatus ;
	}
	
	
	
	}
