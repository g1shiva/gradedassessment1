package com.hcl.collections;
import java.util.*;

public class MagicBook <T>{
		
	    Scanner scanner=new Scanner(System.in);
	    
		 
	    HashMap<Integer,Book> bookmap=new HashMap<>();
		TreeMap<Double,Book> treemap=new TreeMap<>();
		 
		 static Map<Integer, Integer> map = new HashMap<>();
		 
		 
		 ArrayList<Book> booklist=new ArrayList<>();
	   
		public  void addbook() throws IllegalArgumentExceptionDemo{
					
	    	Book books= new Book();
	    	System.out.println("------------------------------\n");
	    	System.out.print("Enter Book ID : ");
	    	books.setId(scanner.nextInt());
	    	
	    	
			System.out.print("Enter a Book Name : ");
			books.setName(scanner.next());

			System.out.print("Enter a Book Price : ");
			books.setPrice(scanner.nextDouble());
		

			System.out.print("Enter a Book Genre : \n1. Poetry\n2. Autobiography\n3. Fiction\n");
			int cc= scanner.nextInt();
			switch (cc) {
			case 1:
				books.setGenre("Poetry     ");
				break;
			case 2:
				books.setGenre("Autobiography");
				break;
			case 3:
				books.setGenre("Fiction    ");
				break;
				
			default :
				System.out.println("Enter the correct option!");
			
			}
			System.out.println();		
			System.out.print("Enter Number of Copies sold : ");
			books.setNoOfCopiesSold(scanner.nextInt());
			

			
			System.out.print("Enter a Book Status : \n1. Available \n2. Sold Out\n");
			int cd= scanner.nextInt();
			switch (cd) {
			case 1:
				books.setBookstatus("Available");
				break;
			case 2:
				books.setBookstatus("Sold Out");
				break;
					
			default :
				System.out.println("Enter the correct option!");
			
			}
			System.out.println();
			bookmap.put(books.getId(), books);
			System.out.println("\nBook added successfully");
			
			
			treemap.put(books.getPrice(), books);
			booklist.add(books);
			
	    }
		
	    
	    public void deletebook() {
	    	
	    	displayBookInfo();
	    	
	    	System.out.print("Enter Book ID which you want to delete : ");
	    	int id=scanner.nextInt();
	    	System.out.println();
	    	bookmap.remove(id);
	    	
	    	System.out.println("Book Details deleted Successfully."+"\n");
	    	 }
	    
	    public void updatebook() {

	    	displayBookInfo();
	    	
	    	Book books= new Book();
	    	System.out.print("Select the Book ID from above table for upadation : ");
	    	books.setId(scanner.nextInt());
	    	
			System.out.print("Enter the updated book name : ");
			books.setName(scanner.next());

			System.out.print("Enter updated price : ");
			books.setPrice(scanner.nextDouble());

			System.out.print("Enter updated genre : ");
			books.setGenre(scanner.next());
			
			System.out.print("Enter updated number of copies sold : ");
			books.setNoOfCopiesSold(scanner.nextInt());
			
			System.out.print("Enter a book status : ");
			books.setBookstatus(scanner.next());
			
			bookmap.replace(books.getId(), books);
			System.out.println("Book details Updated successfully\n");
			
		}
	    
		public void displayBookInfo() {

			if (bookmap.size() > 0) 
			{
				Set<Integer> keySet = bookmap.keySet();
				
				System.out.println("\t     Details of the Books in the store.\t");
				System.out.println("------------------------------------------------------------");
				System.out.println("Id\t"+"Name\t"+"Price\t"+"Genre\t"+"     NoOfCopiesSold\t"+"Book_Status\t");
				System.out.println("------------------------------------------------------------");
				
				for (Integer key : keySet) {

					System.out.println(bookmap.get(key));
				}
				System.out.println("------------------------------------------------------------");
			} 
			else {
				System.out.println("BooksMap is Empty!");
			}

		}

		public void count() {
			
			System.out.print("Number of books present in a store : "+bookmap.size());
			System.out.println();
		}	
		
		public void autobiography() {
		       String genre1 = "Autobiography";
	        
	        ArrayList<Book> genreBookList = new ArrayList<Book>();
		         
		         Iterator<Book> iter=(booklist.iterator());
		         
		         while(iter.hasNext())
		         {
		             Book b1=(Book)iter.next();
		             if(b1.genre.equals(genre1)) 
		             {
		            	 genreBookList.add(b1);
		            	 }             
		         }
		         	System.out.println("  Details of the Books - Under Autobiography genre.\t");
					System.out.println("------------------------------------------------------------");
					System.out.println("Id\t"+"Name\t"+"Price\t"+"Genre\t"+"     NoOfCopiesSold\t"+"Book_Status\t");
					System.out.println("------------------------------------------------------------");
		         for(int i=0;i<genreBookList.size();i++) {
		         System.out.println(genreBookList.get(i));
		         }
		         System.out.println();
		}
		
		public void displayByFeature(int flag) {
			
		if(flag==1)
		{
			if (treemap.size() > 0) 
				{
				  Set<Double> keySet = treemap.keySet();
				  
				  System.out.println("\t Details of the Books - Price Low to Heigh.\t");
					System.out.println("------------------------------------------------------------");
					System.out.println("Id\t"+"Name\t"+"Price\t"+"Genre\t"+"     NoOfCopiesSold\t"+"Book_Status\t");
					System.out.println("------------------------------------------------------------");
					
	   			  for (Double key : keySet) {
					System.out.println(treemap.get(key));
	   			  }
	   			System.out.println("------------------------------------------------------------");
			} else {
						System.out.println("TreeMap is Empty.");
					}
		}
		if(flag==2) 
		{
			 Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
			 // putting values in navigable map
			  NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
			      
			  	      
			   if (nmap.size() > 0) 
			     {
					Set<Double> keySet = nmap.keySet();
					System.out.println("\t Details of the Books - Price High to Low.\t");
					System.out.println("------------------------------------------------------------");
					System.out.println("Id\t"+"Name\t"+"Price\t"+"Genre\t"+"     NoOfCopiesSold\t"+"Book_Status\t");
					System.out.println("------------------------------------------------------------");
					
						for (Double key : keySet) {
								System.out.println(nmap.get(key));
							}
						System.out.println("------------------------------------------------------------");
						
					}else{
						System.out.println("TreeMap is Empty!");
					}
			
		 }
		
		if(flag==3) 
		{
			if (treemap.size() > 0) 
		{
				TreeMap<Integer, Book> sorted = new TreeMap<>();
				sorted.putAll(bookmap);
				
				System.out.println("\tDetails of the Books in Best Selling order.\t");
				System.out.println("------------------------------------------------------------");
				System.out.println("Id\t"+"Name\t"+"Price\t"+"Genre\t"+"     NoOfCopiesSold\t"+"Book_Status\t");
				System.out.println("------------------------------------------------------------");
				
				for (Map.Entry<Integer, Book> entry : sorted.entrySet())
		            System.out.println(entry.getValue());       
		    }
			System.out.println("------------------------------------------------------------\n");
				
		}
	
	
	

		}
}
