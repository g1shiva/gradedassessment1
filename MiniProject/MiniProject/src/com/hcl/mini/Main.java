package com.hcl.mini;




		import java.time.LocalDate;
		import java.time.ZonedDateTime;
		import java.time.format.DateTimeFormatter;
		import java.util.*;

		public class Main {
			public static void main(String[] args) {
				boolean finalOrder;
				
				Scanner sc = new Scanner(System.in);
				Date time=new Date();
				List<Bills> bills = new ArrayList<Bills>();
				List<Items> items = new ArrayList<Items>();

				Items i1 = new Items(1, "Rajma Chawal  ", 2, 150.0);
				Items i2 = new Items(2, "Momos         ", 2, 190.0);
				Items i3 = new Items(3, "Red Thai Curry", 2, 180.0);
				Items i4 = new Items(4, "Chaap         ", 2, 190.0);
				Items i5 = new Items(5, "Chilli Potato ", 1, 250.0);

				items.add(i1);
				items.add(i2);
				items.add(i3);
				items.add(i4);
				items.add(i5);

				
				while (true) {
					System.out.println("\n\t\t   WELCOME TO SURABI RESTAURANT.\n=====================================================================");
					System.out.println("\nPlease Enter the Credentials to Login: ");

					System.out.print("Email    = ");
					String email = sc.next();
					System.out.println();

					System.out.print("Password = ");
					String password = sc.next();
					System.out.println("=====================================================================");

					String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

					System.out.println("Please Enter:\n A or a - if you are Admin\n U or u - if you are User\n L or l - logout");
					String aorU = sc.next();

					Bills bill = new Bills();
					List<Items> selectedItems = new ArrayList<Items>();
					
					double totalCost = 0;
					
					Date date=new Date();
					
					ZonedDateTime time1 = ZonedDateTime.now();
					DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
					String currentTime = time1.format(f);

					if (aorU.equals("U") || aorU.equals("u")) {

						System.out.println("\nWelcome Mr. " + name);
						do {
											
							System.out.println("----------------------- TODAY'S MENU: -------------------------");
							System.out.println("| ITEM CODE "+"\t"+" ITEM NAME"+"\t\t"+"QUANTITY"+"\t"+"PRICE |");
							System.out.println("---------------------------------------------------------------");
							
							items.stream().forEach(i -> System.out.println(i));
							System.out.println("---------------------------------------------------------------");
							System.out.print("\nEnter the Menu Item Code : ");
							int code = sc.nextInt();
							System.out.println();
							
							if (code == 1) {
								selectedItems.add(i1);
								totalCost += i1.getItemPrice();
							}

							else if (code == 2) {
								selectedItems.add(i2);
								totalCost += i2.getItemPrice();
							} 
							else if (code == 3) {
								selectedItems.add(i3);
								totalCost += i3.getItemPrice();
							} 
							else if (code == 4) {
								selectedItems.add(i4);
								totalCost += i4.getItemPrice();
							} 
							else if (code == 5) {
								selectedItems.add(i5);
								totalCost += i5.getItemPrice();
							}
							else {
								System.out.println("You Entered the incorrect option. Please select the correct option!!\n");
							}
							
							System.out.println("Press (0 or 1):\n0. Show bill\n1. Order more");
							int opt = sc.nextInt();
							if (opt == 0)
								finalOrder = false;
							else
								finalOrder =true;

							} 
						
						while (finalOrder);
						

						System.out.println("\nThanks Mr. " + name + " for dining in with Surabi Restaurent. ");
						System.out.println("\nItems you have Ordered.");
						System.out.println("----------------------- SURABI BILL:  -------------------------");
						System.out.println("| ITEM CODE "+"\t"+" ITEM NAME"+"\t\t"+"QUANTITY"+"\t"+"PRICE |");
						System.out.println("---------------------------------------------------------------");
						selectedItems.stream().forEach(e -> System.out.println(e));
						System.out.println("---------------------------------------------------------------");
						System.out.println("Total Bill Amount " + totalCost);
						System.out.println("---------------------------------------------------------------");

						bill.setName(name);
						bill.setCost(totalCost);
						bill.setItems(selectedItems);
						bill.setTime(date);
						bills.add(bill);

					} 
					else if (aorU.equals("A") || aorU.equals("a")) {
						System.out.println("\nWelcome Admin\n");
						System.out.println(
								"Select any option from below:\n1. See all the bills for today.\n2. See all the bills for this month.\n3. See all the bills.\n4. Logout");
						System.out.print("Enter your Option : ");
						
						int option = sc.nextInt();
						System.out.println();
						
						switch (option) {
						case 1:
							if ( !bills.isEmpty()) {
								for (Bills b : bills) {
									if(b.getTime().getDate()==time.getDate()) {
			
									System.out.println("\nUsername : " + b.getName());
									System.out.println("----------------------- SURABI BILL:  -------------------------");
									System.out.println("| ITEM CODE "+"\t"+" ITEM NAME"+"\t\t"+"QUANTITY"+"\t"+"PRICE |");
									System.out.println("---------------------------------------------------------------");
									System.out.println(b.getItems());
									System.out.println("---------------------------------------------------------------");
									System.out.println("Total Bill Amount : " + b.getCost());
									System.out.println("Bill Date         : " + b.getTime());
									System.out.println("---------------------------------------------------------------");
								}
								}
							} 
							else
								System.out.println("No Bills today.!");
							break;

						case 2:
							if (!bills.isEmpty()) {
								for (Bills b : bills) {
									if (b.getTime().getMonth()==time.getMonth()) {
									System.out.println("\nUsername : " + b.getName());
									System.out.println("----------------------- SURABI BILL:  -------------------------");
									System.out.println("| ITEM CODE "+"\t"+" ITEM NAME"+"\t\t"+"QUANTITY"+"\t"+"PRICE |");
									System.out.println("---------------------------------------------------------------");
									System.out.println(b.getItems());
									System.out.println("---------------------------------------------------------------");
									System.out.println("Total Bill Amount : " + b.getCost());
									System.out.println("Bill Date         : " + b.getTime());
									System.out.println("---------------------------------------------------------------");
								}
								}
							} else
								System.out.println("No Bills for this month.!");
							break;

						case 3:
							if (!bills.isEmpty()) {
								for (Bills b : bills) {
									System.out.println("\nUsername : " + b.getName());
									System.out.println(b.getItems());
									System.out.println("Total Bill Amount : " + b.getCost());
									System.out.println("Bill Date         : " + b.getTime());
								}
							} 
							else
								System.out.println("There is no Total Bills.");

							break;
							
						case 4:
							System.out.println("LogOut Successfully.");
							break;

						default:
							System.out.println("Invalid Option, Please select the correct option.");
							System.exit(1);
						}
					} 
					else if (aorU.equals("L") || aorU.equals("l")) {
						System.exit(1);
						
					}
					else  {
						System.out.println("Invalid Entry");
					}

				}

			}
		}

		

	



