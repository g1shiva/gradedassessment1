package com.springboot.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.springboot.main.model.UserBean;
import com.springboot.main.repositry.UserRepositry;

@SpringBootApplication
public class BooksApplication {
	

	@Autowired
	private UserRepositry userRepositry;
	
	

	public static void main(String[] args) {
		SpringApplication.run(BooksApplication.class, args);
	}

	@Bean
	public void insertData() {
		

		UserBean u1 = new UserBean(1,"Kavitha","us", "kavitha@gmail.com", "0620", "9743355135", "India");
		userRepositry.save(u1);

	}
}
