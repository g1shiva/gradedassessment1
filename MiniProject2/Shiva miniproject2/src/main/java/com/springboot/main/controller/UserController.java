package com.springboot.main.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springboot.main.model.UserBean;
import com.springboot.main.repositry.UserRepositry;
import com.springboot.main.services.UserBeanService;

@Controller
public class UserController {

	@Autowired
	private UserBeanService userBeanService;

	@GetMapping("/index")
	public String getIndex() {
		return "index";
	}

	
	@GetMapping("/login")
	public String getLogin(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "login";
	}

	@PostMapping("/login")
	public String postLogin(@RequestParam(required = false) String register, UserBean userBean, HttpSession session) {
		if (register == null) {
			try {
				if (userBeanService.userValidation(userBean)) {
					session.setAttribute("name", userBeanService.getUserBean(userBean.getEmail()).getFirstname());
					session.setAttribute("email",userBeanService.getUserBean(userBean.getEmail()).getEmail());
					session.setAttribute("userId", userBeanService.getUserBean(userBean.getEmail()).getId());
					return "redirect:userdash";
				} else {

					return "redirect:login?error=Invalid credentials!";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				return "redirect:login?error=User not registered!";

			}

		} else {
			return "redirect:register";
		}
	}
	
	@GetMapping("/register")
	public String getRegister(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "register";
	}
	
	@PostMapping("/register")
	public String userRegister(UserBean userBean) {
		System.out.println(userBean);
		if(userBeanService.insertUser(userBean)) {
			return "redirect:login?error=Registration successful!!";
		}else {
			return "redirect:register?error=You have already registered!";
		}
		
	}
	
	@GetMapping("/logout")
	public String getLogout(HttpSession httpSession) {
		httpSession.removeAttribute("email");
		httpSession.removeAttribute("userId");
		httpSession.removeAttribute("name");
		return "login";
	}
	
	@Autowired
	public UserRepositry userRepositry;
	
	
	
	@GetMapping("/userdash")
	public String getuserDashboard( Map<String, List<UserBean>> map) {
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------");
		map.put("contacts", (List<UserBean>) userRepositry.findAll());
		System.out.println(map);
		return "userdash";
	}
	
	
	@GetMapping("/deleteuser")
	public String getDeleteUser(@RequestParam String id) {
		if (userBeanService.deleteUser(id))
		{
			return "redirect:userdash";
		}
		return "redirect:userdash?error";
	}
	
	
	
	@GetMapping("/adduser")
	public String getUser(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "adduser";

	}
	
	@PostMapping("/adduser")
	public String addUsers(UserBean userBean, HttpSession httpSession) {
	
		try {
			if (userBeanService.addUser(userBean)) {
				return "redirect:userdash";
			} else {
				return "redirect:edit?error=user cant be added";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "redirect:edit?error=user cant be added";

		}
	}
	
	@GetMapping("/edituser")
	public String getEditContact(@RequestParam int id, Map<String, UserBean> map) {
		UserBean contactBean = userBeanService.getUser(id);
		System.out.println(contactBean);
		map.put("contact", contactBean);
		return "edituser";
	}

	@PostMapping("/edituser")
	public String postLogin(UserBean contactBean, HttpSession httpSession) {
		try {
			if (userBeanService.updateContact(contactBean)) {

				return "redirect:userdash";
			} else {

				return "redirect:login?error=Editing not possible!!";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "redirect:edituser?error=Editing not possible!!";

		}
	}	

	
	
	
}
