package com.springboot.main.repositry;

import org.springframework.data.repository.CrudRepository;

import com.springboot.main.model.UserBean;

public interface UserRepositry extends CrudRepository<UserBean, Integer>{
     public UserBean findByEmail(String email);
     public boolean existsByEmail(String email);
}
