package com.springboot.main.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.main.model.UserBean;
import com.springboot.main.repositry.UserRepositry;


@Service
public class UserBeanService {
	@Autowired
	private UserRepositry userRepositry;

	public UserBean getUserById(int userId) {
		return this.userRepositry.findById(userId).get();
	}

	public boolean insertUser(UserBean user) {
		if (this.userRepositry.existsByEmail(user.getEmail())) {
			return false;
		}
		this.userRepositry.save(user);
		return true;
	}

	public UserBean getUserBean(String email) throws Exception {
		return this.userRepositry.findByEmail(email);
	}
	
	public UserBean getUser(int id) {
		return this.userRepositry.findById(id).get();
	}

	public boolean userValidation(UserBean user) throws Exception {
		UserBean existUser = null;
		try {
			existUser = getUserBean(user.getEmail());
			if (user.getPassword().equals(existUser.getPassword())) {
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		return false;
	}
	
	public boolean deleteUser(String id) {
		int a = Integer.parseInt(id);
		if (this.userRepositry.existsById(a)) {
			this.userRepositry.deleteById(a);
			return true;
		}
		return false;
	}
	

	public boolean addUser(UserBean contactBean) {
		if (!this.userRepositry.existsById(contactBean.getId())) {
			this.userRepositry.save(contactBean);
			return true;
		}
		return false;
	}
	
	public boolean updateContact(UserBean contactBean) {
		if (this.userRepositry.existsById(contactBean.getId())) {
			this.userRepositry.save(contactBean);
			return true;
		}
		return false;
	}
	
	
	

}
