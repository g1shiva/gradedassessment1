<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register Page</title>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap" rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
    <style media="screen">
      *,
*:before,
*:after{
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}
body{
    background-color: #5F9EA0;
}
.background{
    width: 430px;
    height: 520px;
    position: absolute;
    transform: translate(-50%,-50%);
    left: 50%;
    top: 50%;
}
.background .shape{
    height: 200px;
    width: 200px;
    position: absolute;
    border-radius: 50%;
}
 

form{
    height: 520px;
    width: 500px;
    background-color: #5F9EA0;
    position: absolute;
    transform: translate(-50%,-50%);
    top: 50%;
    left: 50%;


    border: 8px;
    padding: 55px 40px;
}
form * {
    font-family: "Lucida Console", "Courier New", monospace;
    color: black;
    letter-spacing: 0.5px;
    outline: none;
    border: none;
}
form h3{
    font-size: 32px;
    font-weight: 500;
    line-height: 42px;
    text-align: center;
}
 
label{
    display: block;
    margin-top: 30px;
    font-size: 16px;
    font-weight: 500;
}
input{
    display: block;
    height: 50px;
    width: 100%;
    background-color: rgba(255,255,255,0.07);
    border-radius: 3px;
    padding: 0 10px;
    margin-top: 8px;
    font-size: 14px;
    font-weight: 300;
}
::placeholder{
    color: #e5e5e5;
}
button {
    margin-top: 50px;
    width: 100%;
    background-color: white;
    color: black;
    padding: 15px 0;
    font-size: 18px;
    font-weight: 600;

    cursor: pointer;
}
 

    </style>
</head>
<body> <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>

				<form action="edituser" method="post"
					>
				
					<h4 class='text-center text-danger'>${error}</h4>
					<h1 class="first-class" align="center">Edit User Details</h1>
					<h3 class='text-center'>Re-Enter Details</h3>
					<input type="hidden" value="${contact.id}" name="id">
					
						<label for="name">First Name</label> <input type="text"
							class="form-control" id="name" value="${contact.firstname}" placeholder="Enter name"
							name="firstname">
					
					
					
						<label for="name">Last Name</label> <input type="text"
							class="form-control" id="name" value="${contact.lastname}" placeholder="Enter name"
							name="lastname">
					
					
					
					
						<label for="username">Email</label> <input type="text"
							class="form-control" id="email" value="${contact.email}" placeholder="Enter email"
							name="email">
					
					
					
						<label for="username">Password</label> <input type="text"
							class="form-control" id="email" value="${contact.password}" placeholder="Enter email"
							name="password">
					
					
					
						<label for="username">Phone</label> <input type="text"
							class="form-control" id="email" value="${contact.phone}" placeholder="Enter email"
							name="phone">
					
					
						<label for="username">Country</label> <input type="text"
							class="form-control" id="email" value="${contact.country}" placeholder="Enter email"
							name="country">
					
					
					
						<button type="submit"
							class="btn btn-secondary btn-block" name="edituser"
							value="edit">Save Changes</button>
					
				</form>
	

</body>
</html>