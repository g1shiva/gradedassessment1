package com.admin.demo;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import com.admin.demo.model.Admin;
import com.admin.demo.repository.AdminRepository;

@SpringBootApplication
@EnableEurekaClient
public class AdminMicroserviceApplication {
	@Autowired
	private AdminRepository adminRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(AdminMicroserviceApplication.class, args);
		System.out.println("Admin Running on 8000");
	}
	@Bean
public void insertAdmin() {
	Admin admin1 = new Admin("admin","admin@gmail.com","Admin3329#",1);
	Admin admin2 = new Admin("admin1","admin1@gmail.com","Admin3329*",2);
	
	this.adminRepository.save(admin1);
	this.adminRepository.save(admin2);
}

}
