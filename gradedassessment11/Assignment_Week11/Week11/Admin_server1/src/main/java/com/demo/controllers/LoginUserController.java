package com.demo.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.demo.pojo.LoginUser;
import com.demo.service.LoginUserService;

@Controller
public class LoginUserController {

	@Autowired
	private LoginUserService loginUserService;

	@PostMapping("/getRegistered")
	public String getUserRegistered(LoginUser loginUser,HttpSession session) {

		System.out.println("getRegisterDashboardPage is called with " + loginUser);

		if(this.loginUserService.registerUser(loginUser)) {
			System.out.println(this.loginUserService.registerUser(loginUser));
			System.out.println("User is Registered");
			session.setAttribute("mssg", "Your Are Registered Successfully Please Login");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "You are Already Registered");
			return "redirect:/message";
		}
	}

	@GetMapping("/login")
	public String getLoginPage() {
		System.out.println("getLoginPage() is called");
		return "login";
	}

	@PostMapping("/dashboard")
	public String getDashboard(LoginUser loginUser,HttpSession session) {
		System.out.println("getDashboard() is called");

		LoginUser user;
		try {
			user = this.loginUserService.findUser(loginUser);

			if(user.getEmail().equals(loginUser.getEmail()) && user.getPassword().equals(loginUser.getPassword())) {
				session.setAttribute("email",user.getEmail());
				session.setAttribute("name", user.getName());
				session.setAttribute("type", user.getType());
				return "dashboard";
			}else {
				System.out.println("Invalid Password");
				session.setAttribute("mssg", "Invalid Password");
				return "redirect:/message";
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("mssg", "You Are Not A Registered User Please Register");
			return "redirect:/message";
		}
	}

	@GetMapping("/register")
	public String getRegisterPage() {
		System.out.println("getRegisterPage() is called");
		return "register";
	}

	@GetMapping("/logout")
	public String logout(HttpSession session) {
		System.out.println("LoginUser logout() is called");

		session.removeAttribute("email");
		session.removeAttribute("name");
		session.removeAttribute("type");
		session.invalidate();

		return "redirect:/login";
	}

	@GetMapping("/dashboard")
	public String getDashboard() {
		System.out.println("getRegisterPage() is called");
		return "dashboard";
	}
}
