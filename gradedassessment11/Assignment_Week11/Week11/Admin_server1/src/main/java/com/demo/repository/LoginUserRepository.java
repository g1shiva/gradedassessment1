package com.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.pojo.LoginUser;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String>{

}
