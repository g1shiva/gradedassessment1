package com.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.pojo.LoginUser;
import com.demo.repository.LoginUserRepository;

@Service
public class LoginUserService {

	@Autowired
	private LoginUserRepository loginUserRepository;

	public boolean registerUser(LoginUser loginUser) {
		System.out.println(loginUser.getEmail());
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			return false;
		}
		this.loginUserRepository.save(loginUser);
		return true;
	}

	public LoginUser findUser(LoginUser loginUser) throws Exception {
		System.out.println("LoginUser is " + loginUser);
		System.out.println(this.loginUserRepository.findById(loginUser.getEmail()).get());
		return this.loginUserRepository.findById(loginUser.getEmail()).get();
	}
}
