<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
<body>
	<%
	String email = (String) session.getAttribute("email");
	String name = (String) session.getAttribute("name");
	String type = (String) session.getAttribute("type");
	String message = (String) session.getAttribute("mssg");
	%>
	<div class="header" id="headerId" align="center">
		<h1>BOOK STORE</h1>
		<hr>
		
				<%
				if (email != null && type.equals("user")) {
				%><a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a>
				<a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a>
				<a href="like" class="login"><button
							class="btn btn-primary">Liked Books</button></a>
				<a href="read" class="login"><button
							class="btn btn-primary">Read Later Books</button></a>
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a>
				<%
				} else if (email != null && type.equals("admin")) {
				%>
				<a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a>
			<a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a>
				<a href="add" class="login"><button
							class="btn btn-primary">Add Book</button></a>
				<a href="delete" class="login"><button
							class="btn btn-primary">Delete Book</button></a>
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a>
				<%
				} else {
				%>
				<a href="login"
					class="login"><button class="btn btn-primary">Login</button></a>
				<hr>
				<%
				}
				%>
			
		
	</div>
	<%
	if (email != null) {
	%>
	<div id="welcome"align="center">
		
		
			Welcome <strong class="name" title="Your Name"> 
			</strong>
		
	</div>
	<%
	} else {
	%>
	<div id="welcome" align="center">
		<br>
		
			Welcome <strong class="name" title="Your Name"> 
			</strong>
		
	</div>
	<%}%>
	

</html>