package com.demo.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.pojo.Book;
import com.demo.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

	@GetMapping("/add")
	public String addBook() {
		return "add";
	}

	@PostMapping("/addbook")
	public String addBook(Book book,HttpSession session) {
		boolean isAdded = this.bookService.addBookDetails(book);

		if(isAdded) {
			session.setAttribute("mssg", "Book with Name " + book.getBookName() + " Successfully Added");
			return "redirect:/message";
		}else {
			System.out.println("Boook is Already present");
			return "redirect:/message";
		}
	}


	@GetMapping("/delete")
	public String deleteBook() {
		return "delete";
	}

	@GetMapping("/book")
	public String getAllBooks(Map<String,List<Book>> map,HttpSession session) {
		List<Book> listOfAllBooks = bookService.getAllBooks();
		map.put("listOfAllBooks", listOfAllBooks);
		if(listOfAllBooks.isEmpty()) {
			session.setAttribute("mssg", "List is Empty");
			return "redirect:/message";
		}else {
			for(Book book : listOfAllBooks) {
				System.out.println(book);
			}
			return "book";
		}
	}

	@PostMapping("/deletebook")
	public String deleteBookById(@RequestParam("bookId") int id,HttpSession session) {
		boolean isDeleted = this.bookService.deleteBook(id);
		if(isDeleted) {
			session.setAttribute("mssg", "Book With Id " + id + " has been Deleted");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "Book With id " + id + " Does not Exists");
			return "redirect:/message";
		}
	}

	@GetMapping("/edit/{bookId}")
	public String getBookForUpdation(@PathVariable int bookId,Map<String,Integer> map) {
		map.put("bookId", bookId);
		return "edit";
	}

	@PostMapping("/update")
	public String updateBookById(Book book,HttpSession session) {
		System.out.println(book);
		boolean isupdated = this.bookService.updateBook(book);
		if(isupdated) {
			session.setAttribute("mssg", "Book With Id " + book.getBookId() + " has been Updated");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "Book With id " + book.getBookId() + " Does not Exists");
			return "redirect:/message";
		}
	}
}
