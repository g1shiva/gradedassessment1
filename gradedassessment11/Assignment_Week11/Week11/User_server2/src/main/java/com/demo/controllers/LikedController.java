package com.demo.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.pojo.Book;
import com.demo.pojo.Liked;
import com.demo.service.BookService;
import com.demo.service.LikedBookService;

@Controller
public class LikedController {

	@Autowired
	private LikedBookService likedBookService;

	@Autowired
	private BookService bookService;

	@GetMapping("/like")
	public String getLikedBooks(Map<String, List<Liked>> map,HttpSession session){
		List<Liked> listOfLikedBoooks = this.likedBookService.getAllLikedBooks();

		map.put("listOfLikedBoooks", listOfLikedBoooks);

		if(listOfLikedBoooks.isEmpty()) {
			session.setAttribute("mssg", "Liked Book List is Empty");
			return "redirect:/message";
		}else {
			for(Liked liked : listOfLikedBoooks) {
				System.out.println(liked);
			}
			return "liked";
		}
	}

	@RequestMapping(value = "/addliked/{id}",method = RequestMethod.GET)
	public String getBookById(@PathVariable int id,HttpSession session) {
		Book book = this.bookService.getBook(id);
		if(book == null) {
			System.out.println("getBook returns null book "+book);
			session.setAttribute("mssg", "Book Id Does not Exists");
			return "redirect:/message";
		}else {
			Liked liked = new Liked(id, book.getBookName(), book.getBookGenre(),(String)session.getAttribute("email"),book.getBookAuthor(),book.getBookYear());
			boolean isAdded = this.likedBookService.addToLiked(liked);
			if(isAdded){
				System.out.println("Book Added to Liked");
				session.setAttribute("mssg", "Book Added to Liked");
				return "redirect:/message";
			}else {
				session.setAttribute("mssg", "Book is already Present is the Liked Section");
				return "redirect:/message";
			}
		}
	}


	@GetMapping(value = "/removeliked/{id}")
	public String deleteById(@PathVariable int id,HttpSession session){
		boolean isDeleted = this.likedBookService.deleteBook(id);

		if(isDeleted) {
			System.out.println("Book is deleted from liked");
			session.setAttribute("mssg", "Book with id " + id + " Removed From Liked");
			return "redirect:/message";
		}else {
			return "dashboard";
		}
	}

}
