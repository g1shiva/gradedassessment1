package com.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MessageController {

	@GetMapping("/message")
	public String getMessage() {
		System.out.println("getMesage()  called");
		return "message";
	}

}
