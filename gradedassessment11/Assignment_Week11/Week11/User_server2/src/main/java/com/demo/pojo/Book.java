package com.demo.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {

	@Id
	@Column(columnDefinition = "int(10)")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookId;

	@Column(columnDefinition = "varchar(30)")
	private String bookName;

	@Column(columnDefinition = "varchar(30)")
	private String bookGenre;

	@Column(columnDefinition = "varchar(30)")
	private String bookAuthor;

	@Column(columnDefinition = "varchar(30)")
	private String bookYear;

	public Book() {
		System.out.println("Book Class Constructor");
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public String getBookYear() {
		return bookYear;
	}

	public void setBookYear(String bookYear) {
		this.bookYear = bookYear;
	}

	public Book( String bookName, String bookGenre, String bookAuthor, String bookYear) {
		super();

		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.bookAuthor = bookAuthor;
		this.bookYear = bookYear;
	}

	public Book(int bookId, String bookName, String bookGenre, String bookAuthor, String bookYear) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.bookAuthor = bookAuthor;
		this.bookYear = bookYear;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + ", bookAuthor="
				+ bookAuthor + ", bookYear=" + bookYear + "]";
	}


}
