package com.demo.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ReadLater {

	@Id
	@Column(columnDefinition = "int(10)")
	private int bookId;

	@Column(columnDefinition = "varchar(30)")
	private String bookName;

	@Column(columnDefinition = "varchar(30)")
	private String bookGenre;

	@Column(columnDefinition = "varchar(30)")
	private String email;

	@Column(columnDefinition = "varchar(30)")
	private String bookAuthor;

	@Column(columnDefinition = "varchar(30)")
	private String bookYear;



	public ReadLater() {

	}



	public ReadLater(int bookId, String bookName, String bookGenre, String email, String bookAuthor, String bookYear) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.email = email;
		this.bookAuthor = bookAuthor;
		this.bookYear = bookYear;
	}



	public int getBookId() {
		return bookId;
	}



	public void setBookId(int bookId) {
		this.bookId = bookId;
	}



	public String getBookName() {
		return bookName;
	}



	public void setBookName(String bookName) {
		this.bookName = bookName;
	}



	public String getBookGenre() {
		return bookGenre;
	}



	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getBookAuthor() {
		return bookAuthor;
	}



	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}



	public String getBookYear() {
		return bookYear;
	}



	public void setBookYear(String bookYear) {
		this.bookYear = bookYear;
	}



	@Override
	public String toString() {
		return "ReadLater [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + ", email="
				+ email + ", bookAuthor=" + bookAuthor + ", bookYear=" + bookYear + "]";
	}



}
