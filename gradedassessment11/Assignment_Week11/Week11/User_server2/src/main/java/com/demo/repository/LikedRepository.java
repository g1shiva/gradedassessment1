package com.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.pojo.Liked;

@Repository
public interface LikedRepository extends CrudRepository<Liked, Integer>{

}
