package com.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.pojo.Book;
import com.demo.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;


	public List<Book> getAllBooks() {

		List<Book> listOfBooks = new ArrayList<>();
		this.bookRepository.findAll().forEach((books -> listOfBooks.add(books)));
		return listOfBooks;
	}

	public Book getBook(int id) {
		return this.bookRepository.findById(id).get();
	}

	public boolean addBookDetails(Book book) {
		if(this.bookRepository.existsById(book.getBookId())) {
			return false;
		}
		this.bookRepository.save(book);
		return true;
	}

	public boolean deleteBook(int id) {
		if(this.bookRepository.existsById(id)) {
			this.bookRepository.deleteById(id);
			return true;
		}
		return false;
	}

	public boolean updateBook(Book book) {
		if(this.bookRepository.existsById(book.getBookId())) {
			this.bookRepository.save(book);
			return true;
		}
		return false;
	}
}
