package com.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.pojo.ReadLater;
import com.demo.repository.ReadLaterRepository;

@Service
public class ReadLaterBookService{

	@Autowired
	private ReadLaterRepository readLaterRepository;


	public List<ReadLater> getAllReadLaterBooks() {
		List<ReadLater> listOfReadLaterBooks = new ArrayList<>();
		this.readLaterRepository.findAll().forEach((readLater->listOfReadLaterBooks.add(readLater)));
		return listOfReadLaterBooks;
	}

	public boolean addToReadLater(ReadLater readLater) {
		if(this.readLaterRepository.existsById(readLater.getBookId())) {
			return false;
		}
		this.readLaterRepository.save(readLater);
		return true;
	}

	public boolean deleteBook(int id) {
		if(this.readLaterRepository.existsById(id)) {
			this.readLaterRepository.deleteById(id);
			return true;
		}
		return false;
	}
}
