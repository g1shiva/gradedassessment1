<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU"
	crossorigin="anonymous">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
	crossorigin="anonymous"></script>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css"
	rel="stylesheet" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<style>


.header {
	background-color: green;
	color: white;
	height: 100px;
	margin-top: -7px;
	margin-left: -15px;
	width: 101%;
	position: fixed;
}

.title {
	font-family: Arial, Helvetica, sans-serif;
	display: inline-block;
	text-align: center;
	margin-top: 15px;
	font-size: 40px;
	margin-left: 15px;
	height: 50%;
	padding: 5px;
}

.navLink {
	display: inline-block;
	margin-left: 45%;
}

.navUL {
	list-style-type: none;
}

li {
	display: inline;
}

a {
	color: white;
	text-decoration: none;
}

.li {
	margin-left: 10px;
}

.cardforbook1 {
	border: 2px solid blue;
	border-radius: 10px;
	width: 400px;
	text-align: center;
	margin-left: 3%;
	height: 300px;
	float: left;
}

.cardforbook1:hover {
	transform: scale(1.10);
	cursor: pointer;
}

.upperdiv {
	background-color: rgb(44, 92, 197);
	height: 20px;
	border-top-left-radius: 10px;
	border-top-right-radius: 10px;
	height: 70px;
	margin-left: -2px;
	margin-top: -2px;
	margin-right: -2px;
}

img {
	width: 90px;
	height: 90px;
	border-radius: 50%;
	margin-top: -45px;
}

.id {
	padding: 5px;
	text-align: center;
	margin: 5px;
	font-size: 18px;
	width: 100%;
	color: white;
	font-size: 40px;
}

hr {
	margin: 2px;
}

.btnLeft {
	width: 30%;
	float: left;
	margin-left: 15%;
	margin-top: 40px;
	height: 70px;
	font-size: 30px;
	margin-bottom: 50px;
}

.btnright {
	margin-left: 10%;
	width: 30%;
	margin-top: 40px;
	margin-right: 10%;
	height: 70px;
	font-size: 30px;
	margin-bottom: 50px;
}

.id2 {
	padding: 5px;
	text-align: right;
	margin: 5px;
	font-size: 18px;
	width: 40%;
	color: white;
}

.cardforbook2 {
	border: 2px solid blue;
	border-radius: 10px;
	width: 400px;
	text-align: center;
	margin-right: 3%;
	height: 300px;
	float: right;
}

.cardforbook2:hover {
	
	cursor: pointer;
}

.cardforbook3 {
	border: 2px solid blue;
	border-radius: 10px;
	width: 400px;
	text-align: center;
	margin: auto;
	height: 300px;
}

.cardforbook3:hover {
	transform: scale(1.10);
	cursor: pointer;
}

.wrap {
	margin-top: 30px;
	padding: 5px;
	margin-left: 100px;
	border-radius: 5px;
	height: auto;
}

.data {
	font-size: 40px;
}

.mark {
	font-size: 40px;
	padding: 5px;
	margin-left: 10px;
	margin-right: 10px;
	margin-top: 110px;
	
	border-radius: 10px;
	color: white;
}

.num {
	margin-top: -5px;
	font-size: 55px;
	color: black;
	background-color: black;
	display: block;
	margin-bottom: 10px;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	height: 90px;
	text-align: center;
	color: white;
	width: 100.5%;
}

img {
	width: 90px;
	height: 90px;
	border-radius: 50%;
	margin-top: -54px;
	margin-left: 125px;
}

.cardTitle {
	margin-left: 40%;
}

.txt {
	text-align: center;
}

.mytxt {
	margin-left: 50px;
}

.brdr {
	border: 2px solid black;
	width: 350px;
}

.crd {
	transform: scale(1.05);
}

#welcome{

height:100px;
background-color:white;
}

</style>
</head>
<body>
	<%
	String email = (String) session.getAttribute("email");
	String name = (String) session.getAttribute("name");

	String type = (String) session.getAttribute("type");
	%>

	<div class="header" id="headerId">
	<br>
		<div align="center">
		<h1>BOOK STORE</h1>
		</div>
		<div class="navLink">
			<ul class="navUL">
				<%
				if (email != null && type.equals("user")) {
				%>
				
				<li class="li"><a href="dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book"><button
							class="btn btn-success">Show Books</button></a></li>
				<li class="li"><a href="like" class="login"><button
							class="btn btn-primary">Liked Books</button></a></li>
				<li class="li"><a href="read" class="login"><button
							class="btn btn-primary">Read Later Books</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%
				} else if(email != null && type.equals("admin")){
				%>
				<li class="li"><a href="dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="add"><button
							class="btn btn-success">Add Books</button></a></li>
				<li class="li"><a href="delete"><button
							class="btn btn-success">Delete Book</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%
				}else{
				%>
				<li class="li"><a href="login" class="login"><button
							class="btn btn-primary">Login</button></a></li>
				<%
				}
				%>
			</ul>
		</div>
	</div>

	<%
	if (email != null) {
	%>
	<div id="welcome" align="center"><br><br><br><br><br>
	<h1>	Welcome Mr/Mrs. <strong class="name" title="Your Name">
			<%=name%>
		</strong></h1>
		<hr>
	</div>
	<%
	} else {
	%>
	<div id="welcome" align="center"><br><br><br><br>
	<h1>	Welcome Mr/Mrs. <strong class="name" title="Your Name">
			<%=name%>
		</strong></h1>
		<hr>
	</div>

	<%
	}
	
	%>
	<div class="wrap">
	<br>
	<br>
	<br>
		<c:forEach items="${listOfAllBooks}" var="bookList">
			<div class="crd col-md-3 m-a-2">
				<div class="brdr card">
					
					<ul class="txt list-group list-group-flush cole">
						<li class="list-group-item cole">Book Id : <strong>${bookList.getBookId()}</strong></li>
						<li class="list-group-item cole">Book Name : <strong>${bookList.getBookName()}</strong></li>
						<li class="list-group-item cole">Book Genre : <strong>${bookList.getBookGenre()}</strong></li>
						<li class="list-group-item cole">Book Author : <strong>${bookList.getBookAuthor()}</strong></li>
						<li class="list-group-item cole">Book Year : <strong>${bookList.getBookYear()}</strong></li>
					</ul>
					<div class="card-block cole">
						<%
						if (email != null && type.equalsIgnoreCase("user")) {
						%>
						<a href="addliked/${bookList.getBookId()}"><button type="button"
								class="mytxt btn btn-primary btn-responsive">Like</button></a> <a
							href="addread/${bookList.getBookId()}"><button type="button"
								class="mytxt btn btn-primary btn-responsive">Read Later</button></a>
						<%
						} else if (email != null && type.equalsIgnoreCase("admin")) {
						%>
						<a href="edit/${bookList.getBookId()}" style="margin-left: 25%;"><button
								type="button" class="mytxt btn btn-primary btn-responsive">Edit</button></a>
						<%
						} else {
						%>
					
						<%
						}
						%>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>