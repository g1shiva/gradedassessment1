<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<style>
.header {
	background-color: green;
	color: white;
	height: 100px;
	margin-top: -7px;
	margin-left: -15px;
	width: 101%;
}

.title {
	font-family: Arial, Helvetica, sans-serif;
	display: inline-block;
	text-align: center;
	margin-top: 15px;
	font-size: 40px;
	margin-left: 15px;
	height: 50%;
	padding: 5px;
}

.navLink {
	display: inline-block;
	margin-left: 50%;
}

.navUL {
	list-style-type: none;
}

li {
	display: inline;
}

a {
	color: white;
	text-decoration: none;
}

.li {
	margin-left: 10px;
}

.mark {
	font-size: 40px;
	padding: 5px;
	margin: 10px;
	border-radius: 10px;
	color: white;
}

.cardLogin {
	margin-left: 30%;
	margin-top: 20px;
	border: 2px solid black;
	display: inline-block;
	width: 35%;
	height: 250px;
	border-radius: 10px;
}

#welcome {
	height: 100px;
	background-color: white;
}

.loginTitle {
	background-color: black;
	text-align: center;
	font-size: 30px;
	border-top-left-radius: 8px;
	border-top-right-radius: 8px;
	height: 90px;
	font-size: 50px;
	padding: 5px;
	color: white;
}

.userName {
	margin: 10px;
	padding: 5px;
	font-family: Arial, Helvetica, sans-serif;
}

.inp {
	height: 50px;
	color: black
}

.uname {
	margin-left: 150px;
}

p {
	font-size: 30px;
	margin-right: 100px;
	display: inline-block;
}

.radiobtn {
	font-size: 30px;
}

input[type=radio] {
	height: 20px;
	width: 20px;
}

.lab {
	margin-bottom: 5px;
	font-size: 20px;
}

.name {
	background-color: red;
	color: white;
}

.name:hover {
	background-color: white;
	color: black;
	cursor: pointer;
}
</style>
<body>
	<%
	String email = (String) session.getAttribute("email");
	String name = (String) session.getAttribute("name");
	%>
	<div class="header" id="headerId" align="center">
	<br>
		<h1>BOOK STORE</h1>
		<div class="navLink">
			<ul class="navUL">
				<%
				if (email != null) {
				%>
				<li class="li"><a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book" class="login"><button
							class="btn btn-primary">Show Books</button></a></li>
				<li class="li"><a href="add" class="login"><button
							class="btn btn-primary">Add Book</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%}%>
			</ul>
		</div>
	</div>
	<div id="welcome" align="center">
	<hr>
		<br>
		<h1>
			Welcome Mr/Mrs. <strong class="name" title="Your Name"> <%=name%>
			</strong>
		</h1>
		<hr>
</div>

		<form action="deletebook" method="POST" >
			<div class="cardLogin">
				<div class="loginTitle">Delete Book Details</div>
				<div class="form-group userName">
					<label for="name">Book Id</label> <input type="text"
						class="form-control inp" id="name" placeholder="Enter Book Id"
						name="bookId">
				</div>
				<div align="center">
					<button type="submit" class="btn btn-success" value="Login">Delete
						</button>
				</div>
			</div>
		</form>
</body>
</html>