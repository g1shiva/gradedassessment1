package com.demo;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.pojo.Book;
import com.demo.pojo.LoginUser;
import com.demo.repository.BookRepository;
import com.demo.repository.LoginUserRepository;
import com.demo.service.BookService;
import com.demo.service.LoginUserService;

@SpringBootTest
class ShivaKumarUserBookGradedW11ApplicationTests {

	@Test
	void contextLoads() {
	}
	

	@InjectMocks
	LoginUserService loginUserService;

	@InjectMocks
	BookService bookService;

	@Mock
	BookRepository bookRepository;

	@Mock	
	LoginUserRepository loginUserRepository;

	@Test
	public void addUserTest() {
		LoginUser loginUser=new LoginUser();
		loginUserService.registerUser(loginUser);
		verify(loginUserRepository,times(1)).save(loginUser);
	}

	@Test
	public void getUserTest() {
		List<LoginUser> list = new ArrayList<LoginUser>();
		LoginUser l1 = new LoginUser();
		LoginUser l2 = new LoginUser();
		list.add(l1);
		list.add(l2);
		when(loginUserRepository.findAll().equals(list));
		String uL = loginUserService.toString();
		assertEqual(2,uL.equalsIgnoreCase(uL));
		verify(loginUserRepository.findAll());

	}

	private void assertEqual(int i, boolean equalsIgnoreCase) {
		// TODO Auto-generated method stub
	}
	
	@Test
	public void addBook() {
		Book book = new Book();
		bookService.addBookDetails(book);
		verify(bookRepository,times(1)).save(book);
	}
	
	@Test
	public void addBook1() {
		Book book = new Book();
		bookService.addBookDetails(book);
		verify(bookRepository,times(1)).save(book);
	}

	@Test
	public void creBok() {
		Book b=new Book("jas","rss","dd","2011");
		b.setBookAuthor("dd");
		verify(bookRepository,times(1)).deleteById(1);
	}

}
