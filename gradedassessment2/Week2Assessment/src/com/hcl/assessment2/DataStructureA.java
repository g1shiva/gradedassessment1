package com.hcl.assessment2;


import java.util.ArrayList;
public class DataStructureA {

	public void sortingNames(ArrayList<Employee> employees) {
		employees.sort((name1, name2) -> name1.getName().compareTo(name2.getName()));
		
		System.out.println("Names of all Employees in the Sorted Order :");
		System.out.print("[ ");
		for (Employee emp0 : employees) {
			System.out.print(emp0.getName() + " ");
		}
		System.out.println("]");
	}
	
	
	
	
	
}
