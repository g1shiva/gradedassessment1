package com.hcl.assessment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;


public class DataStructureB {

	public void cityNameCount(ArrayList<Employee> employees) {
		Set<String> sl = new TreeSet<String>();
        ArrayList<String> al = new ArrayList<String>();

        for (Employee emp : employees) {
           	al.add(emp.getCity());
            sl.add(emp.getCity());
        }
        
        System.out.println("Count of Employees from each City:");
        System.out.print("{ ");
        for (String city_name : sl) {
            System.out.print(city_name + "=" + Collections.frequency(al, city_name) + " ");
        }
        System.out.println("}");
        
     }

     public void monthlySalary(ArrayList<Employee> employees) {
    	 System.out.println("Monthly Salary of employee along with their ID: ");
    	 System.out.print("{ ");
         for (Employee emp2 : employees) {
              System.out.print(emp2.getId() + "=" + emp2.getSalary() / 12 + " ");
         }
         System.out.println("}");
     }

	
	
	
}
