package com.hcl.assessment2;

public class Employee {

	int id;
	String name;
    int age;
    int salary; 
    String department;
    String city;
//Getters and Setters Methods
    public int getId() {
    	return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    
 @Override
 	public String toString() {
        return "  "+id +"\t"+name+"\t  "+age+"\t"+salary+"\t\t  "+department+"\t\t"+city;
    }
	
	
	
}
