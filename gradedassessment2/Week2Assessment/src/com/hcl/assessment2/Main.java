package com.hcl.assessment2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		
		 ArrayList<Employee> employees = new ArrayList<>();
     	Employee e1 = new Employee(1, "Aman", 21, 1100000, "IT", "Delhi");
         employees.add(e1);

         Employee e2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay");
         employees.add(e2);

         Employee e3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi");
         employees.add(e3);

         Employee e4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");
         employees.add(e4);

         Employee e5 = new Employee(5, "Smitha", 24, 1200000, "HR", "Bengaluru");
         employees.add(e5);
         
 public Employee(int id, String name, int age, int salary, String department, String city)throws IllegalArgumentExceptionDemo {
    	super();
    	
    	if(id<0)
    	{
    		throw new IllegalArgumentExceptionDemo("exception occured, id cannot less than zero");
    	}
        this.id = id;

        if(name==null) {
        	throw new IllegalArgumentExceptionDemo("exception occured, name cannot be null");
        }
        this.name = name;

        if(age<=0)
        {
        	throw new IllegalArgumentExceptionDemo("exception occured, age cannot be zero");
        }
        this.age = age;

        if(salary<0)
        {
        	throw new IllegalArgumentExceptionDemo("exception occured, salary cannot be zero");
        }
        this.salary = salary;

        if( department == null) 
        {
        	throw new IllegalArgumentExceptionDemo("exception occured, department cannot be null");
        }
        this.department = department;

        if(city==null)
        {
            throw new IllegalArgumentExceptionDemo("exception occured, city cannot be null");
        }
        this.city = city;
    
    }

         while(true) {
         	
         	Scanner select = new Scanner(System.in);	
         	System.out.println("EMPLOYEE RECORDS:"+"\n"+"---------------------------------------------"+"\n"+"Select any option from below: "+"\n"+"1. Display List of all Employees."+"\n"+"2. Display the Names of Employees in Sorted order."+"\n"+"3. Display the count of Employees from each city."+"\n"+"4. Display Monthly salary of each Employee."+"\n"+"5. Display All."+"\n"+"6. Exit"+"\n");
         	System.out.print("Enter your option: ");
 	        int select_option = select.nextInt();
 	        
 	        switch(select_option) {
 	        	case 1:
 	        		//for list of employees
 	            	System.out.println("\n"+"List of Employees:"+"\n\n"+"S.No"+"\t"+" NAME"+"\t"+"  AGE"+"\t"+"SALARY(INR)"+"\t"+"DEPARTMENT"+"\t"+"LOCATION");
 	            	System.out.println("-----------------------------------------------------------------");
 	            	for(int i=0;i<5;i++) { 
 	            		System.out.println(employees.get(i));
 	            	}
 	            	System.out.println("-----------------------------------------------------------------");
 	            	System.out.println();
 	            	break;
 	            
 	        	case 2:
 	        		//for displaying names in sorted order.
 	            	DataStructureA a = new DataStructureA();
 	                
 	            	System.out.println();
 	            	a.sortingNames(employees);
 	            	System.out.println();
 	            	break;
 	            	
 	        	case 3:
 	        		//for displaying count of employees from each city
 	        		 DataStructureB b = new DataStructureB();
 	                 
 	        		 System.out.println();
 	                 b.cityNameCount(employees);
 	                 System.out.println();
 	                 break;
 	                 
 	        	case 4:
 	        		//for monthly salary displaying
 	        		DataStructureB c = new DataStructureB();
 	        		
 	        		System.out.println();
 	        		c.monthlySalary(employees);
 	                System.out.println();
 	                break;
 	                
 	        	case 5:
 	        		//for displaying all information
 	        		System.out.println("\n"+"List of Employees:"+"\n\n"+"S.No"+"\t"+" NAME"+"\t"+"  AGE"+"\t"+"SALARY(INR)"+"\t"+"DEPARTMENT"+"\t"+"LOCATION");
 	            	System.out.println("-----------------------------------------------------------------");
 	            	for(int i=0;i<5;i++) { 
 	            		System.out.println(employees.get(i));
 	            	}
 	            	System.out.println("-----------------------------------------------------------------");
 	            	System.out.println();
 	            	
 	            	DataStructureA d = new DataStructureA();
 	                
 	            		d.sortingNames(employees);
 	            		System.out.println();
 	            		
 	            	DataStructureB e = new DataStructureB();
    	                 
 	            		e.cityNameCount(employees);
    	                 	System.out.println();
    	                 	e.monthlySalary(employees);
    	                 	System.out.println();
    	                 	break;
    	                 	
 	        	 case 6:
 	        		 //for exit option.
 	 	        	System.exit(1);
 	 	        
 	 	         default:
 	 	        	System.out.println("Please Enter the valid option."+"\n");
 	            	
	
 	        }
         	
         }
	} 
}
		
		
		
		
		

	

