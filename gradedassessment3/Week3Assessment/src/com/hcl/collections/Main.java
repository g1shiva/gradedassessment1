package com.hcl.collections;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
public static void Main(String[] args) {
		
		Scanner scanner=new Scanner(System.in);
	    MagicBook BookName=new MagicBook();
	   
		while (true) {
			System.out.println("\t\t MAGIC OF BOOKS\t\t\n"+"=============================================");
			System.out.println("1. Add a New Book.\n"
					+ "2. Delete a Book.\n" + "3. Update a Book.\n"
					+ "4. Display all books. \n" + "5. Total count of books.\n"
					+ "6. Search books under Autobiography genre." +  "\n7. Arrange books by particular Order.");
			System.out.println("=============================================");
			System.out.print("\nEnter your choice : ");
			int choice = scanner.nextInt();
			System.out.println();

			switch (choice) {

			case 1://For Adding a book1
				System.out.print("Enter Number of books you want to add : ");
				int n=scanner.nextInt();
				System.out.println();
				
				for(int i=1;i<=n;i++) {
					BookName.addbook();
					}
				break;
			
			case 2://Delete a book
				BookName.deletebook();
				    break;
	
			case 3://Update a book
				BookName.updatebook();
				break;

			case 4://for displaying books details
				BookName.displayBookInfo();
				break;
			
			case 5://for counting number of books in the store
				System.out.println("Count of all books-");
				BookName.count();
				break;
				
			case 6:
				BookName.autobiography();
			      break;
			      
			
			case 7:
				System.out.println("Enter your choice:\n 1. Price low to high "
						+ "\n 2. Price high to low \n 3. Best selling");
				int ch = scanner.nextInt();

				switch (ch) {

				case 1:  BookName.displayByFeature(1);
				         break;
				case 2:  BookName.displayByFeature(2); 
				         break;
				case 3:  BookName.displayByFeature(3);
						break;
				default :
					System.out.println("You enterted the wrong Option!\n");
				}
				break;
			
			default:
				System.out.println("Entered wrong choice! Please select the correct option.\n");

			}

		}

      }
		
		
		
		

	}


