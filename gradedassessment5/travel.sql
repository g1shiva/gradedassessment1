create database travel;
use travel;
create table PASSENGER
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);

create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

select count(CASE WHEN (Gender) = 'M' THEN 1 END) Male,
count(CASE WHEN (Gender) = 'F' THEN 1 END) Female from travel.passenger where Distance>=600;

select min(price) from travel.price where Bus_Type='sleeper';

select *from travel.passenger where Passenger_name like 's%';

select p1.Passenger_name,p1.Boarding_city,p1.Destination_city,p1.Bus_TYpe,p2.Price from travel.passenger p1,travel.price p2 where p1.Distance=p2.Distance and p1.Bus_type=p2.Bus_type;

select p1.Passenger_name,p1.Boarding_city,p1.Destination_city,p1.Bus_TYpe,p2.Price from travel.passenger p1,travel.price p2 where p1.Distance=1000 and p1.Bus_type="sitting";

select distinct p1.Passenger_name, p1.Boarding_city as Destination_city,p1.Destination_city as Boarding_city, p1.Bus_Type, p2.Price from travel.passenger p1,travel.price p2 where Passenger_name="Pallavi" and p1.Distance=p2.Distance;

select distinct distance from travel.passenger order by Distance desc;

SELECT passenger_name,distance,distance*100/t.d as "percentage" from passenger cross join (select sum(Distance) as d from passenger)t;

create view p_view as select *from travel.passenger where Category='AC';
select * from p_view;

DELIMITER &&
CREATE PROCEDURE Slipper_Travellers()
BEGIN
SELECT * FROM passenger where Bus_Type ='Sleeper';
END &&

select *from travel.passenger limit 5;