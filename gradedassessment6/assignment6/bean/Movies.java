package com.greatlearning.assignment6.bean;

public interface Movies {
	public int getId();
	public void setId(int id);
	public String getTitle();
	public void setTitle(String title);
	public int getYear() ;
	public void setYear(int year);
	public String getGenres();
	public void setGenres(String genres);
	public int getRatings();
	public void setRatings(int ratings);
	public String getPoster() ;
	public void setPoster(String poster);
	public int getContentRating();
	public void setContentRating(int contentRating);
	public String getDuration();
	public void setDuration(String duration);
	public int getAverageRating();
	public void setAverageRating(int averageRating);
	public String getStoryLine();
	public void setStoryLine(String storyLine);
	public float getImdbRating();
	public void setImdbRating(float imdbRating);
}
