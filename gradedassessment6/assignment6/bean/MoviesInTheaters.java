package com.greatlearning.assignment6.bean;


public class MoviesInTheaters implements Movies{
	
	private int id;
	private String title;
	private int year;
	private String genres;
	private int ratings;
	private String poster;
	private int contentRating;
	private String duration;
	private int averageRating;
	private String storyLine;
	private float imdbRating;
	
	private MoviesInTheaters() {
		super();
		
	}
	private static MoviesInTheaters movietheater = new MoviesInTheaters();
	public static  MoviesInTheaters getInstance() {
		return movietheater;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public int getRatings() {
		return ratings;
	}
	public void setRatings(int ratings) {
		this.ratings = ratings;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	public int getContentRating() {
		return contentRating;
	}
	public void setContentRating(int contentRating) {
		this.contentRating = contentRating;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public int getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}
	public String getStoryLine() {
		return storyLine;
	}
	public void setStoryLine(String storyLine) {
		this.storyLine = storyLine;
	}
	public float getImdbRating() {
		return imdbRating;
	}
	public void setImdbRating(float imdbRating) {
		this.imdbRating = imdbRating;
	}
	@Override
	public String toString() {
		return "MoviesInTheaters [id=" + id + ", title=" + title + ", year=" + year + ", genres=" + genres
				+ ", ratings=" + ratings + ", poster=" + poster + ", contentRating=" + contentRating + ", duration="
				+ duration + ", averageRating=" + averageRating + ", storyLine=" + storyLine + ", imdbRating="
				+ imdbRating + "]";
	}
	
	
	

}
