package com.greatlearning.assignment6.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.db.DBConnection;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class MoviesComingDao {
		private MoviesComingDao() {
			super();
		}
		
		private static final MoviesComingDao  moviecomdao = new MoviesComingDao();
		
		public static MoviesComingDao getInstance() {
			return moviecomdao;
		
		}

		public List<Movies> typeMoviesComing(){
			List<Movies> moviesList = new ArrayList<>();
			try {
				
				Connection con = DBConnection.getDBConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from moviescoming");
				ResultSet result = pstmt.executeQuery();
				while(result.next()) {
					Movies movie =MoviesOnTipsFactory.getInstance("Movies Coming");
					movie.setId(result.getInt(1));
					movie.setTitle(result.getString(2));
					movie.setYear(result.getInt(3));
					movie.setPoster(result.getString(6));
					moviesList.add(movie);
				}
			
			}catch(Exception e) {
				System.out.println("In Movies Coming method "+e);
			}
			
			return moviesList;
		}
}
