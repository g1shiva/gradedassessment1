package com.greatlearning.assignment6.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.db.DBConnection;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class MoviesInTheatersDao {
		private MoviesInTheatersDao() {
			super();
		}
		
		private static final MoviesInTheatersDao  movietheadao = new MoviesInTheatersDao();
		
		public static MoviesInTheatersDao getInstance() {
			return movietheadao;
		
		}

		public List<Movies> typeMovieInTheaters(){
			List<Movies> moviesList = new ArrayList<>();
			try {
				Connection con = DBConnection.getDBConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from Moviesintheaters");
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()) {
					Movies movie =MoviesOnTipsFactory.getInstance("Movies In Theaters");
					movie.setId(rs.getInt(1));
					movie.setTitle(rs.getString(2));
					movie.setYear(rs.getInt(3));
					moviesList.add(movie);
				}
				
			}catch(Exception e) {
				System.out.println("In Movies in Theaters method "+e);
			}
			
			return moviesList;
		}

}
