package com.greatlearning.assignment6.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.db.DBConnection;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class TopRatedIndiaDao {
		private TopRatedIndiaDao() {
			super();
		}
		
		private static final TopRatedIndiaDao  topindiadao = new TopRatedIndiaDao();
		
		public static TopRatedIndiaDao getInstance() {
			return topindiadao;
		
		}
		
		public List<Movies> typeTopRatedIndiaMovies(){
			List<Movies> listOfMovies = new ArrayList<>();
			try {
				Connection con = DBConnection.getDBConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from topratedindia");
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()) {
					Movies movie =MoviesOnTipsFactory.getInstance("Top Rated India");
					movie.setId(rs.getInt(1));
					movie.setTitle(rs.getString(2));
					movie.setYear(rs.getInt(3));
					movie.setImdbRating(rs.getFloat(11));
					listOfMovies.add(movie);
				}
				
			}catch(Exception e) {
				System.out.println("In Top rated India "+e);
			}
			
			return listOfMovies;
		}

}
