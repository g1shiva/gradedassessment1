package com.greatlearning.assignment6.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.db.DBConnection;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class TopRatedMoviesDao{
		private TopRatedMoviesDao() {
			super();
		}
		
		private static final TopRatedMoviesDao  topmoviesdao = new TopRatedMoviesDao();
		
		public static TopRatedMoviesDao getInstance() {
			return topmoviesdao;
		
		}

		
		public List<Movies> typeTopRatedMovies(){
			List<Movies> listOfMovies = new ArrayList<>();
			try {
				Connection con = DBConnection.getDBConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from topratedmovies");
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()) {
					Movies movie =MoviesOnTipsFactory.getInstance("Top Rated Movies");
					movie.setId(rs.getInt(1));
					movie.setTitle(rs.getString(2));
					movie.setYear(rs.getInt(3));
					listOfMovies.add(movie);
				}
				
			}catch(Exception e) {
				System.out.println("In Top Rated movies method "+e);
			}
			
			return listOfMovies;
		}
		


}
