package com.greatlearning.assignment6.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	private static Connection connec;
	
	
	private DBConnection() {	
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connec = DriverManager.getConnection("jdbc:mysql://localhost:3306/assignment6_aravindsamrat_nalam", "root", "Samrat@123");
		} 
		catch (Exception e) {
			System.out.println("Database-Connection Error " + e);
		}

	}
		
	private static final DBConnection instance = new DBConnection();
	public static DBConnection getInstance() {
		return instance;
	}
	
	public static Connection getDBConnection() {
		try {
			return connec;
		}
		catch(Exception e) {
		}
		return null;
	}
}