package com.greatlearning.assignment6.factory;
import com.greatlearning.assignment6.bean.*;

public class MoviesOnTipsFactory {

		public static Movies getInstance(String type) {     //the instance of the class
			
			if(type.equalsIgnoreCase("Movies Coming")) {
				return MoviesComing.getInstance();				//instance of MoviesComing class
			}
			
			else if(type.equalsIgnoreCase("Movies In Theaters")) {
				return  MoviesInTheaters.getInstance();
			}
			
			else if(type.equalsIgnoreCase("Top Rated India")) {
				return  TopRatedIndia.getInstance();
			}
			
			else if(type.equalsIgnoreCase("Top Rated Movies")) {
				return  TopRatedMovies.getInstance();
			}
			
			else {
				return null;
		}
	}
}

