package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;
import org.junit.Test;
import java.sql.Connection;

import com.greatlearning.assignment6.db.DBConnection;

public class DBConnectionTest {

	@Test
	public void testGetInstance() {
		DBConnection dbinst = DBConnection.getInstance();
		assertNotNull(dbinst);
		
	}

	@Test
	public void testGetDBConnection() {
		Connection connec = DBConnection.getDBConnection();
		assertNotNull(connec);
		
	}

}
