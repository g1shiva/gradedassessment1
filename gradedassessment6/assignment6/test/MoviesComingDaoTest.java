package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;
import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.dao.MoviesComingDao;


public class MoviesComingDaoTest {

	@Test
	public void testTypeMoviesComing() {
		MoviesComingDao movcom = MoviesComingDao.getInstance();
		List<Movies> movieList =movcom.typeMoviesComing();
		
		assertEquals(3, movieList.size());
		
	}

}
