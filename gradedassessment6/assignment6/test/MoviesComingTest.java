package com.greatlearning.assignment6.test;
import com.greatlearning.assignment6.factory.*;

import static org.junit.Assert.*;
import org.junit.Test;
import com.greatlearning.assignment6.bean.*;

public class MoviesComingTest {

	@Test
	public void testGetId() {
		Movies movie = MoviesOnTipsFactory.getInstance("Movies coming");
		movie.setId(2);
		assertTrue(movie.getId() == 2);
	}

	@Test
	public void testSetId() {
		Movies movie = MoviesOnTipsFactory.getInstance("Movies coming");
		movie.setId(4);
		assertTrue(movie.getId() == 4);
	}

	@Test
	public void testGetTitle() {
		Movies movie = MoviesOnTipsFactory.getInstance("Movies coming");
		movie.setTitle("The Lodgers");
		assertTrue(movie.getTitle() == "The Lodgers");
	}

	@Test
	public void testGetDuration() {
		Movies movie = MoviesOnTipsFactory.getInstance("Movies coming");
		movie.setDuration("PT95M");
		assertEquals("PT95M",movie.getDuration());
	}

	@Test
	public void testSetDuration() {
		Movies movie = MoviesOnTipsFactory.getInstance("Movies coming");
		movie.setDuration("PT88M");
		assertEquals("PT88M",movie.getDuration());
		
	}
	
	@Test
	public void testGetStoryLine() {
		Movies movie = MoviesOnTipsFactory.getInstance("Movies coming");
		movie.setStoryLine("A claustrophobic survival thriller set beneath the Yellow Sea off the coast of North Korea where the pilot of a small submersible craft and a three man Special Ops team on a secret recovery mission become trapped underwater in a fight for survival.");
		assertTrue(movie.getStoryLine() == "A claustrophobic survival thriller set beneath the Yellow Sea off the coast of North Korea where the pilot of a small submersible craft and a three man Special Ops team on a secret recovery mission become trapped underwater in a fight for survival.");
				
	}
}
