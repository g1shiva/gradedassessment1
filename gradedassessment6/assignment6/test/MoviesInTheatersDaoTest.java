package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.assignment6.bean.Movies;
import com.greatlearning.assignment6.dao.*;

public class MoviesInTheatersDaoTest {

	@Test
	public void testTypeMovieInTheaters() {
		MoviesInTheatersDao movcom = MoviesInTheatersDao.getInstance();
		List<Movies> movieList =movcom.typeMovieInTheaters();
		
		assertEquals(1, movieList.size());
	}

}
