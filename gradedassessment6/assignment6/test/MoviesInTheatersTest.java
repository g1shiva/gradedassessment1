package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class MoviesInTheatersTest {

	@Test
	public void testGetYear() {
		Movies moviesintheater = MoviesOnTipsFactory.getInstance("Movies in theaters");
		moviesintheater.setYear(2018);
		assertTrue(moviesintheater.getYear() == 2018);
	}

	@Test
	public void testGetPoster() {
		Movies moviesintheater = MoviesOnTipsFactory.getInstance("Movies in theaters");
		moviesintheater.setPoster("MV5BNzhmNmI5MDMtZDEyOC00ODkyLTkwODItODQzODAzY2QyZjVlXkEyXkFqcGdeQXVyMzQwMTY2Nzk@._V1_SY500_CR0,0,357,500_AL_.jpg");
		assertEquals("MV5BNzhmNmI5MDMtZDEyOC00ODkyLTkwODItODQzODAzY2QyZjVlXkEyXkFqcGdeQXVyMzQwMTY2Nzk@._V1_SY500_CR0,0,357,500_AL_.jpg",moviesintheater.getPoster());		
	}

	@Test
	public void testSetPoster() {
		Movies moviesintheater = MoviesOnTipsFactory.getInstance("Movies in theaters");
		moviesintheater.setPoster("MV5BNzhmNmI5MDMtZDEyOC00ODkyLTkwODItODQzODAzY2QyZjVlXkEyXkFqcGdeQXVyMzQwMTY2Nzk@._V1_SY500_CR0,0,357,500_AL_.jpg");
		assertTrue(moviesintheater.getPoster() == "MV5BNzhmNmI5MDMtZDEyOC00ODkyLTkwODItODQzODAzY2QyZjVlXkEyXkFqcGdeQXVyMzQwMTY2Nzk@._V1_SY500_CR0,0,357,500_AL_.jpg");
	}

	@Test
	public void testSetDuration() {
		Movies moviesintheater = MoviesOnTipsFactory.getInstance("Movies in theaters");
		moviesintheater.setDuration("PT95M");
		assertEquals("PT95M",moviesintheater.getDuration());
	}

	@Test
	public void testGetAverageRating() {
		Movies moviesintheater = MoviesOnTipsFactory.getInstance("Movies in theaters");
		moviesintheater.setAverageRating(0);
		assertTrue(moviesintheater.getAverageRating() == 0);
	}
}
