package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;
import org.junit.Test;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;
import com.greatlearning.assignment6.bean.Movies;

public class MoviesOnTipsFactoryTest {

	@Test
	public void testGetInstance() {
		Movies moviefactory = MoviesOnTipsFactory.getInstance("Movies in Theaters");
		assertNotNull(moviefactory);
	}

}
