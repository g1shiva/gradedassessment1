package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.assignment6.bean.Movies;
import com.greatlearning.assignment6.dao.TopRatedIndiaDao;

public class TopRatedIndiaDaoTest {

	@Test
	public void testTypeTopRatedIndiaMovies() {
		TopRatedIndiaDao movcom = TopRatedIndiaDao.getInstance();
		List<Movies> movieList =movcom.typeTopRatedIndiaMovies();
		
		assertEquals(1, movieList.size());
	}

}
