package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class TopRatedIndiaTest {

	@Test
	public void testSetYear() {
		Movies topratedindia  = MoviesOnTipsFactory.getInstance("Top Rated India");
		topratedindia.setYear(2016);
		assertTrue(topratedindia.getYear() == 2016);
	}

	@Test
	public void testGetDuration() {
		Movies topratedindia  = MoviesOnTipsFactory.getInstance("Top Rated India");
		topratedindia.setDuration("PT101M");
		assertTrue(topratedindia.getDuration() == "PT101M");
	}

	@Test
	public void testGetAverageRating() {
		Movies topratedindia  = MoviesOnTipsFactory.getInstance("Top Rated India");
		topratedindia.setAverageRating(1);
		assertTrue(topratedindia.getAverageRating() == 1);		
	}

	@Test
	public void testGetStoryLine() {
		Movies topratedindia  = MoviesOnTipsFactory.getInstance("Top Rated India");
		topratedindia.setStoryLine("A claustrophobic survival thriller.");
		assertEquals("A claustrophobic survival thriller.",topratedindia.getStoryLine());
	}

	@Test
	public void testSetImdbRating() {
		Movies topratedindia  = MoviesOnTipsFactory.getInstance("Top Rated India");
		topratedindia.setImdbRating((float) 7.0);
		assertTrue(topratedindia.getImdbRating() == 7.0);		
	}
}
