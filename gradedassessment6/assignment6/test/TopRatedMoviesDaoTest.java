package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.assignment6.bean.Movies;
import com.greatlearning.assignment6.dao.*;

public class TopRatedMoviesDaoTest {

	@Test
	public void testTypeTopRatedMovies() {
		TopRatedMoviesDao movcom = TopRatedMoviesDao.getInstance();
		List<Movies> movieList =movcom.typeTopRatedMovies();
		
		assertEquals(1, movieList.size());
	}

}
