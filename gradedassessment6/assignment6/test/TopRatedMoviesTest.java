package com.greatlearning.assignment6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.assignment6.bean.*;
import com.greatlearning.assignment6.factory.MoviesOnTipsFactory;

public class TopRatedMoviesTest {
	@Test
	public void testSetTitle() {
		Movies topratedmovies  = MoviesOnTipsFactory.getInstance("Top Rated Movies");
		topratedmovies.setTitle("Red Sparrow");
		assertTrue(topratedmovies.getTitle() == "Red Sparrow");
	}

	@Test
	public void testGetYear() {
		Movies topratedmovies  = MoviesOnTipsFactory.getInstance("Top Rated Movies");
		topratedmovies.setYear(2016);
		assertTrue(topratedmovies.getYear() == 2016);
		
	}

	@Test
	public void testGetDuration() {
		Movies topratedmovies  = MoviesOnTipsFactory.getInstance("Top Rated Movies");
		topratedmovies.setDuration("PT101M");
		assertTrue(topratedmovies.getDuration() == "PT101M");
	}

	@Test
	public void testSetStoryLine() {
		Movies topratedmovies  = MoviesOnTipsFactory.getInstance("Top Rated Movies");
		topratedmovies.setStoryLine("This 2017 movie follows the original dance academy TV show and tracks where the characters are in their lives now.");
		assertTrue(topratedmovies.getStoryLine() == "This 2017 movie follows the original dance academy TV show and tracks where the characters are in their lives now.");
	}

	@Test
	public void testSetImdbRating() {
		Movies topratedmovies  = MoviesOnTipsFactory.getInstance("Top Rated Movies");
		topratedmovies.setImdbRating((float) 7.0);
		assertTrue(topratedmovies.getImdbRating() == 7.0);
		
	}

}
