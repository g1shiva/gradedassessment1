package week7.greatlearning.beans;

public class Book {
private int id;
private String name;
private String author;
private String image;

public Book() {
	super();
	// TODO Auto-generated constructor stub
}

public Book(int id, String name, String author, String image) {
	super();
	this.id = id;
	this.name = name;
	this.author = author;
	this.image = image;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}

@Override
public String toString() {
	return "Book [id=" + id + ", name=" + name + ", author=" + author + ", image=" + image + "]";
}

}
