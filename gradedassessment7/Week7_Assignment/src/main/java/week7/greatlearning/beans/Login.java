package week7.greatlearning.beans;

public class Login {
private String emailId;
private String password;


public Login(String emailId, String password) {
	super();
	this.emailId = emailId;
	this.password = password;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

@Override
public String toString() {
	return "login [emailId=" + emailId + ", password=" + password +  "]";
}
public Login() {
	super();
	// TODO Auto-generated constructor stub
}


}
