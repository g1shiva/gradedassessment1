package week7.greatlearning.beans;

public class SignUp {
private String emailId;
private String password;
public SignUp() {
	super();
	// TODO Auto-generated constructor stub
}


public SignUp(String emailId, String password) {
	super();
	this.emailId = emailId;
	this.password = password;
}

public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
@Override
public String toString() {
	return "SignUp [emailId=" + emailId + ", password="
			+ password + "]";
}


}
