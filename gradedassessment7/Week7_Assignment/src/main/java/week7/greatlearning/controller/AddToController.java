package week7.greatlearning.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddToController
 */
@WebServlet("/AddToController")
public class AddToController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddToController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
		HttpSession session=request.getSession();
		List li;
		if(session.getAttribute("login")!=null) {
			String param=request.getParameter("id");
		
		if(session.getAttribute("Like")==null) {
			li=new ArrayList();
			li.add(param);
			session.setAttribute("Like", li);
			
		}else {
			li=(List)session.getAttribute("Like");
			if(!li.contains(param)) {
			li.add(param);
			}
			session.setAttribute("Like", li);
		}
		response.getWriter().print(li.size());
		}
		RequestDispatcher rd=request.getRequestDispatcher("Liked.jsp");
		rd.include(request,response);
	}
}
