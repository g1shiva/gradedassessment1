package week7.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import week7.greatlearning.beans.Login;


public class LoginDao {
	private String url="jdbc:mysql://localhost:3306/mamatha_week7";
    private String name="root";
    private String pass="123456";
    private String driver="com.mysql.cj.jdbc.Driver";
    public void DbLoadDriver(String driver) {
    	try {
   		 Class.forName(driver);
   		 
   	}catch(Exception e) {
   		System.out.println(" "+e);
   	}
    }
    
    public Connection getConnection() {
    	 Connection con=null;
    	try {
    		
   		    con=DriverManager.getConnection(url,name,pass);
   		    
    		
    	}catch(Exception e) {
    		System.out.println("Exception caught"+e);
    	}
    	return con;
    }
	 
	 public String storeLoginInfo(Login login) {
		    DbLoadDriver(driver);
			Connection con = getConnection();
			String result="data stored successfully";
			try {
				PreparedStatement pstmt = con.prepareStatement("insert into login values(?,?)");
				pstmt.setString(1,login.getEmailId());
				pstmt.setString(2,login.getPassword());
				
				if(login.getEmailId()==null && login.getPassword()==null) {
					System.out.println("sorry! you have entered wrong credentials");
				}else
				{
					pstmt.executeUpdate();
				}
			}catch(Exception e) {
				System.out.println("Store Method Exception "+e);
				result="record not stored";
			}
			return result;
	 }
    

// 
    
}
