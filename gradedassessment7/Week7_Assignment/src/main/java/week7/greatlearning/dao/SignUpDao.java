package week7.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import week7.greatlearning.beans.SignUp;


public class SignUpDao {
        private String url="jdbc:mysql://localhost:3306/mamatha_week7";
        private String name="root";
        private String pass="123456";
        private String driver="com.mysql.cj.jdbc.Driver";
        public void DbLoadDriver(String driver) {
        	try {
       		 Class.forName(driver);
       		 
       	}catch(Exception e) {
       		System.out.println(" "+e);
       	}
        }
        
        public Connection getConnection() {
        	 Connection con=null;
        	try {
        		
       		    con=DriverManager.getConnection(url,name,pass);
       		    
        		
        	}catch(Exception e) {
        		System.out.println("Exception caught"+e);
        	}
        	return con;
        }
	public String storeSignUp(SignUp sp) {
		DbLoadDriver(driver);
		Connection con = getConnection();
		String result="record stored successfully";
		try {
			
			PreparedStatement pstmt = con.prepareStatement("insert into registration values(?,?)");
			pstmt.setString(1, sp.getEmailId());
			pstmt.setString(2,sp.getPassword());
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			result="record not stored";
		}
		return result;
	}
}
