package com.greatlearning.week8.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.greatlearning.week8.bean.*;

public class BooksDAO {

	@Autowired
	private JdbcTemplate template;

	public List<Books> getAllBooks() {
		
		String sql = "select id, name , author, url from Books";
		return template.query(sql, new BookRowMapper());
		
	}
}		