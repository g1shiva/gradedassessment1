package com.greatlearning.week8.dao;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.greatlearning.week8.bean.*;

public class LoginDAO {
	
	
	@Autowired
	private JdbcTemplate template;

	public boolean validateUser(String username, String password) {
		String sql = "select username, password from BookUsers where username=? and password=?";
		List<BookUsers> user = template.query(sql, new LoginRowMapper(), username , password);
		if(user.size()==0)
			return false;
		else
			return true;
		
	}

}
