package com.greatlearning.week8.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.greatlearning.week8.bean.*;

public class ReadLaterDAO {
	
	@Autowired
	private JdbcTemplate template;
	

	public boolean addBooks(int id, String name, String author, String url) throws DuplicateKeyException {
		String sql = "insert into ReadLater values(?,?,?,?)";
		int i  = template.update(sql, id , name , author , url);
		if (i!=0)
			return true;
		else
			throw new DuplicateKeyException("Book Already Exists");
	}
		
		public List<ReadLaterBooks> getAllBooks() {
			String sql = "select id, name , author , url from ReadLater";
			return template.query(sql, new ReadLaterRowMapper());
		}
	


	public boolean deleteBook(int id) {
		String sql = "delete from ReadLater where id = ?";
		int i = template.update(sql, id);
		if(i!=0)
			return true;
		else
			return false;
	}


}
