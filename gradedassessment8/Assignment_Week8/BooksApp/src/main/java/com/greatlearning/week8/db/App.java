package com.greatlearning.week8.db;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.greatlearning.week8.bean.*;
import com.greatlearning.week8.dao.*;
import com.greatlearning.week8.service.*;

@Configuration
public class App {
	
	@Bean
    public JdbcTemplate template() {
       	JdbcTemplate template = new JdbcTemplate();
    	template.setDataSource(ds());
    	return template;   	
    }

	@Bean
    public DriverManagerDataSource ds() { 	
    	DriverManagerDataSource ds = new DriverManagerDataSource();
    	ds.setDriverClassName("com.mysql.cj.jdbc.Driver");;
    	ds.setUrl("jdbc:mysql://localhost:3306/book1_aravindsamrat");
    	ds.setUsername("root");
    	ds.setPassword("Samrat@123"); 	
    	return ds;
    	
    }
    
	@Bean
	@Scope(value = "prototype")
	public Books book() {
		return new Books();
	}
	
	@Bean
	@Scope(value = "prototype")
	public LikedBooks likedBook() {
		return new LikedBooks();
	}
	
	@Bean
	@Scope(value = "prototype")
	public ReadLaterBooks readLaterBook() {
		return new ReadLaterBooks();
	}
	
	@Bean
	public RegisterService service() {
		return new RegisterService();
	}
	
	@Bean
	public LoginService service1() {
		return new LoginService();
	}

	@Bean
	public BooksService service3() {
		return new BooksService();
	}
	
	@Bean
	public LikedBooksService service4() {
		return new LikedBooksService();
	}
	
	@Bean
	public ReadLaterService service5() {
		return new ReadLaterService();
	}
	
	@Bean
	public BooksDAO dao() {
		return new BooksDAO();
	}
	
	@Bean
	public LikedDAO likedDao() {
		return new LikedDAO();
	}
	
	@Bean
	public LoginDAO loginDao() {
		return new LoginDAO();
	}
		
	@Bean
	public ReadLaterDAO readLaterDao() {
		return new ReadLaterDAO();
	}
	
	@Bean
	public RegisterDAO registerDao() {
		return new RegisterDAO();
	}	
}
