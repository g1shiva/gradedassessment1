package com.greatlearning.week8.service;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.greatlearning.week8.dao.*;

public class RegisterService {

	@Autowired
	RegisterDAO dao ;

	public boolean registerUser(String username, String password) throws DuplicateKeyException {
		return dao.registerUser(username, password);
	}
}
