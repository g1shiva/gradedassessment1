<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Read Later</title>
<style>
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
    
    table {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        border-collapse: collapse;
        width: 800px;
        height: 200px;
        border: 1px solid #bdc3c7;
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    
    h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }
    
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h1>BOOKS STORE APPLICATION - READ LATER BOOKS</h1><hr>
	<center>
	<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	out.print("<h4>User Name :  " + username + " </h4><br/ >");
	%>
	<input type='button' value='Logout'
			onclick="location.href= 'login'">
			<hr>
			</center>
	
	<table border="2" width="70%" cellpadding = "2" align ="center">
		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Author</b></th>
			<th><b>Image</b></th>		
		</tr>
		<c:forEach var="i" items="${books}">
			<tr align = "center">
					<td><c:out value="${i.getId() }"></c:out></td>
					<td><c:out value="${i.getName() }"></c:out></td>
					<td><c:out value="${i.getAuthor() }"></c:out></td>
					<td><img src=<c:out value="${i.getUrl() }"></c:out>width="80" height="75"></td>
   					<td><a href="readLaterDelete/${i.getId()}">Remove</a></td>  			
			</tr>
		</c:forEach>
	</table>
	<br/><br/>
	
</body>
</html>