<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration</title>
<style>
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
    
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    
    h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }
    
     @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
		<h1>BOOKS STORE APPLICATION</h1><hr>
		<br/><center>
	<h2>User Registration</h2>
	<form action="register" method="post">
		Username: <input type="text" name="username" /> <br> <br>
		Password: <input type="password" name="password" /><br> <br>
		<input type="submit" value="Register">
	</form>
	<br /><hr>
	<br /> Already have Account!! <a href="login">Login</a>
	</center>
</body>
</html>