<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
    
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    
    h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }
    
     @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
<h1>BOOKS STORE APPLICATION</h1>
<br/><br/><center>
<h2>Welcome Mr/Mrs. <strong> ${username} </strong> Account is successfully Registered. 
<br/> <br/>
Please Login.</h2><hr>  
<a href ="login">Login </a>
</center>
</body>
</html>