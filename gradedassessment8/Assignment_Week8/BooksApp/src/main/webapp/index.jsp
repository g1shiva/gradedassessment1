<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<style>
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
    
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    
    h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }
    
     @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ page import="java.util.*"%>
	<%@ page import="com.greatlearning.week8.bean.*"%>

	<h1>BOOKS STORE APPLICATION</h1><hr>
	<center>
	<img src="https://2020.scottishdesignawards.com/wp-content/uploads/2020/03/ThePortobelloBookshop0032-1680x1120.jpg" width="800" height="360"><br/><br/>
    <button onclick="location.href='login'">Login</button>     <button  
    onclick="location.href='register'">Register</button>

	<br /><br /><br />
	<form action="home" method="post">
		<button type="submit">Show Books without login</button>
	</form>
	</center>
</body>
</html>