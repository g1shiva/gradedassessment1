create database book1_aravindsamrat;
use book1_aravindsamrat;

create table BookUsers(
username varchar(30) primary key  ,
password varchar(30)
);

create table Books(
id int primary key,
name varchar(30),
author varchar(30),
url varchar(800)
);

insert into books values(1, "The Innovators Dilemma", "Clayton Chrisrensen","https://venturebeat.com/wp-content/uploads/2012/04/innovators-dilemma.jpg?resize=200%2C265&strip=all");
insert into books values(2, "The Soul of a new Machine",  "Tracy Kidder","https://venturebeat.com/wp-content/uploads/2012/04/the-soul-of-a-new-machine.jpg?resize=200%2C267&strip=all");
insert into books values(3, "Andy Grove",  "Richard","https://venturebeat.com/wp-content/uploads/2012/04/andy-grove.jpg?resize=200%2C267&strip=all");
insert into books values(4, "The New New",  "Michael Lewis","https://venturebeat.com/wp-content/uploads/2012/04/the-new-new-thing.jpg?resize=200%2C260&strip=all");
insert into books values(5, "The world is Flat", "Thomas Friedman","https://venturebeat.com/wp-content/uploads/2012/04/the-world-is-flat.jpg?resize=200%2C282&strip=all");
insert into books values(6, "The Chip", "TR Reid","https://venturebeat.com/wp-content/uploads/2012/04/the-chip.jpg?resize=200%2C267&strip=all");
insert into books values(7, "Hackers",  "Steven Levy","https://venturebeat.com/wp-content/uploads/2012/04/hackers.jpg?resize=200%2C264&strip=all");
insert into books values(8, "Microcosm",  "George Gilder","https://venturebeat.com/wp-content/uploads/2012/04/microcosm.jpg?resize=200%2C284&strip=all");
insert into books values(9, "Telecosm", "George Gilder","https://venturebeat.com/wp-content/uploads/2012/04/telecosm.jpg?resize=200%2C271&strip=all");
insert into books values(10, "The Wisdom of Crowds",  "James Surowiecki","https://venturebeat.com/wp-content/uploads/2012/04/the-wisdom-of-crowds.jpg?resize=200%2C277&strip=all");


create table ReadLater(
id int primary key,
name varchar(30),
author varchar(30),
url varchar(800)
);


create table Likelist(
id int primary key,
name varchar(30),
author varchar(30),
url varchar(800)
);