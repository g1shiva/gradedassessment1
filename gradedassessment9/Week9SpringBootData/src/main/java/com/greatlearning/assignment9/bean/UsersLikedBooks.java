package com.greatlearning.assignment9.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userslikedbooks")
public class UsersLikedBooks {
	@Id
	@Column(name="id")
	private int id;
	private String name;
	private String author;
	private String type;
	private String image;
	
	
	public UsersLikedBooks() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public UsersLikedBooks(int id, String name, String author, String type, String image) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.type = type;
		this.image = image;
		
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	


	@Override
	public String toString() {
		return "UsersLikedBooks [id=" + id + ", name=" + name + ", author=" + author + ", type=" + type + ", image="
				+ image + ",]";
	}
	

	
}
