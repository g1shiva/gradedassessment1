package com.greatlearning.assignment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.assignment9.bean.AdminLoginAndLogout;
import com.greatlearning.assignment9.bean.AdminSignUp;
import com.greatlearning.assignment9.service.AdminLoginAndLogoutService;

@RestController
@RequestMapping(value="/adminloggs")
public class AdminLoginAndLogoutController {

	@Autowired
	AdminLoginAndLogoutService adminloginandlogoutService;
	
	@GetMapping(value="getAllLogin",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<AdminLoginAndLogout> getAllUsersInfo(){
		 return adminloginandlogoutService.getAllAdmin();
	 }
	 
	 @PostMapping(value="storeLogin",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeUsers(@RequestBody AdminLoginAndLogout ad) {
		 return adminloginandlogoutService.storeAdminInfo(ad);
	 }
	 
	 @DeleteMapping(value="deleteLogins&Logout/{id}")
	 public String deleteBooks(@PathVariable("id")int id) {
		 return adminloginandlogoutService.deleteLoginInfo(id);
		}
}
