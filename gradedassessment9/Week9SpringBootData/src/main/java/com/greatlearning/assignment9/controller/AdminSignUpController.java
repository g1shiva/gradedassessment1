package com.greatlearning.assignment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.assignment9.bean.AdminSignUp;
import com.greatlearning.assignment9.bean.Users;
import com.greatlearning.assignment9.service.AdminSignUpService;

@RestController
@RequestMapping(value="/admin")
public class AdminSignUpController {

	@Autowired
	AdminSignUpService adminsignupService;
	
	@GetMapping(value="getAllAdmin",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<AdminSignUp> getAllUsersInfo(){
		 return adminsignupService.getAllAdmin();
	 }
	 
	 @PostMapping(value="storeAdmin",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeUsers(@RequestBody AdminSignUp ad) {
		 return adminsignupService.storeAdminInfo(ad);
	 }
}
