package com.greatlearning.assignment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.assignment9.bean.Book;
import com.greatlearning.assignment9.bean.UsersLikedBooks;
import com.greatlearning.assignment9.service.UsersLikedBooksService;

@RestController
@RequestMapping(value="/liked")
public class UsersLikedBooksController {

	@Autowired
	UsersLikedBooksService usersLikedBooksService;
	
	@GetMapping(value="getAllBook",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<UsersLikedBooks> getAllProductInfo(){
		 return usersLikedBooksService.getAllBooks();
	 }
	 
	 @PostMapping(value="storeBook",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeProduct(@RequestBody UsersLikedBooks uld) {
		 return  usersLikedBooksService.storeBookInfo(uld);
	 }
}
