package com.greatlearning.assignment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.assignment9.bean.AdminLoginAndLogout;

@Repository
public interface AdminLoginAndLogoutDao extends JpaRepository<AdminLoginAndLogout, Integer>{

}
